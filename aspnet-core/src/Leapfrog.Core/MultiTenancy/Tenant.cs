﻿using Abp.MultiTenancy;
using Leapfrog.Authorization.Users;

namespace Leapfrog.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
