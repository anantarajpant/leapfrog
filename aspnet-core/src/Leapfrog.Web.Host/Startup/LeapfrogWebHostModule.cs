﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Leapfrog.Configuration;

namespace Leapfrog.Web.Host.Startup
{
    [DependsOn(
       typeof(LeapfrogWebCoreModule))]
    public class LeapfrogWebHostModule: AbpModule
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public LeapfrogWebHostModule(IWebHostEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(LeapfrogWebHostModule).GetAssembly());
        }
    }
}
