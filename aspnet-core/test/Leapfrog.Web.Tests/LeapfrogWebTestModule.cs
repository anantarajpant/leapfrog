﻿using Abp.AspNetCore;
using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Leapfrog.EntityFrameworkCore;
using Leapfrog.Web.Startup;
using Microsoft.AspNetCore.Mvc.ApplicationParts;

namespace Leapfrog.Web.Tests
{
    [DependsOn(
        typeof(LeapfrogWebMvcModule),
        typeof(AbpAspNetCoreTestBaseModule)
    )]
    public class LeapfrogWebTestModule : AbpModule
    {
        public LeapfrogWebTestModule(LeapfrogEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbContextRegistration = true;
        } 
        
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(LeapfrogWebTestModule).GetAssembly());
        }
        
        public override void PostInitialize()
        {
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(LeapfrogWebMvcModule).Assembly);
        }
    }
}