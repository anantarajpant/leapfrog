﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Leapfrog.Sessions.Dto;

namespace Leapfrog.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
