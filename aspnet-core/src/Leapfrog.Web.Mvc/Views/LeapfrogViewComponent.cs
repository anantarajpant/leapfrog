﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace Leapfrog.Web.Views
{
    public abstract class LeapfrogViewComponent : AbpViewComponent
    {
        protected LeapfrogViewComponent()
        {
            LocalizationSourceName = LeapfrogConsts.LocalizationSourceName;
        }
    }
}
