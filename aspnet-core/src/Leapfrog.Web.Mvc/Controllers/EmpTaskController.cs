﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Leapfrog.Authorization;
using Leapfrog.Common;
using Leapfrog.Controllers;
using Leapfrog.Task;
using Leapfrog.Task.Dtos;
using Leapfrog.Web.Models.People;
using Leapfrog.Web.Models.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Leapfrog.Web.Controllers
{
    public class EmpTaskController : LeapfrogControllerBase
    {
        private readonly ITaskAppService _taskAppService;
        private readonly IEmpTaskAppService _empTaskAppService;

        public EmpTaskController(
            ITaskAppService taskAppService,
            IEmpTaskAppService empTaskAppService)
        {
            _taskAppService = taskAppService;
            _empTaskAppService = empTaskAppService;
        }

        public async Task<ActionResult> Index(GetAllTasksInput input)
        {
            var output = await _taskAppService.GetAll(input);

            var model = new IndexViewModel(output.Items)
            {
                SelectedTaskState = input.State
            };

            return View(model);
        }

        public async Task<ActionResult> Create()
        {
            var peopleSelectListItems = (await _empTaskAppService.GetPeopleComboboxItems()).Items
                .Select(p => p.ToSelectListItem())
                .ToList();

            peopleSelectListItems.Insert(0, new SelectListItem { Value = string.Empty, Text = L("Unassigned"), Selected = true });

            return View(new CreateTaskViewModel(peopleSelectListItems));
        }
    }
}
