﻿using System.Collections.Generic;
using Leapfrog.Roles.Dto;

namespace Leapfrog.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}
