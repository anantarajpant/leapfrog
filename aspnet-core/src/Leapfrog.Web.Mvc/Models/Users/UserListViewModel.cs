using System.Collections.Generic;
using Leapfrog.Roles.Dto;

namespace Leapfrog.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}
