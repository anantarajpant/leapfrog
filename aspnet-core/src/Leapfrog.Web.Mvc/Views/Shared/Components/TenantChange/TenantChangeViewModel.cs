﻿using Abp.AutoMapper;
using Leapfrog.Sessions.Dto;

namespace Leapfrog.Web.Views.Shared.Components.TenantChange
{
    [AutoMapFrom(typeof(GetCurrentLoginInformationsOutput))]
    public class TenantChangeViewModel
    {
        public TenantLoginInfoDto Tenant { get; set; }
    }
}
