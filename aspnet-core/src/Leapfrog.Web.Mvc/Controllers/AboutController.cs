﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using Leapfrog.Controllers;

namespace Leapfrog.Web.Controllers
{
    [AbpMvcAuthorize]
    public class AboutController : LeapfrogControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}
