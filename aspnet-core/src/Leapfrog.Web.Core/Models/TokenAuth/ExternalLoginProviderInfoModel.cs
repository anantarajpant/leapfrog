﻿using Abp.AutoMapper;
using Leapfrog.Authentication.External;

namespace Leapfrog.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
