﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using Leapfrog.Configuration.Dto;

namespace Leapfrog.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : LeapfrogAppServiceBase, IConfigurationAppService
    {
        public async System.Threading.Tasks.Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }

        
    }
}
