using System.ComponentModel.DataAnnotations;

namespace Leapfrog.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}