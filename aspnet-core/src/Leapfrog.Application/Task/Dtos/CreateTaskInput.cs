﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Leapfrog.Task.Dtos
{
    [AutoMapTo(typeof(Leapfrog.Tasks.Task))]
    public class CreateTaskInput
    {
        [Required]
        [MaxLength(Leapfrog.Tasks.Task.MaxTitleLength)]
        public string Title { get; set; }

        [MaxLength(Leapfrog.Tasks.Task.MaxDescriptionLength)]
        public string Description { get; set; }

        public Guid? AssignedPersonId { get; set; }
    }
}
