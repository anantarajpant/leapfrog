﻿using Abp.Authorization;
using Leapfrog.Authorization.Roles;
using Leapfrog.Authorization.Users;

namespace Leapfrog.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
