using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace Leapfrog.EntityFrameworkCore
{
    public static class LeapfrogDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<LeapfrogDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<LeapfrogDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
