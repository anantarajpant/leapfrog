﻿using Abp.Application.Services.Dto;

namespace Leapfrog.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

