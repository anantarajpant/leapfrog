﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using Leapfrog.Authorization.Roles;
using Leapfrog.Authorization.Users;
using Leapfrog.MultiTenancy;
using Leapfrog.Tasks;

namespace Leapfrog.EntityFrameworkCore
{
    public class LeapfrogDbContext : AbpZeroDbContext<Tenant, Role, User, LeapfrogDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public DbSet<Task> Tasks { get; set; }
        public LeapfrogDbContext(DbContextOptions<LeapfrogDbContext> options)
            : base(options)
        {
        }
    }
}
