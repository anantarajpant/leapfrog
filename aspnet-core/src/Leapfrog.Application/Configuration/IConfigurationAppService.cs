﻿using System.Threading.Tasks;
using Leapfrog.Configuration.Dto;

namespace Leapfrog.Configuration
{
    public interface IConfigurationAppService
    {
        System.Threading.Tasks.Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
