﻿using Leapfrog.Tasks;
using System;
using System.Collections.Generic;
using System.Text;

namespace Leapfrog.Task.Dtos
{
    public class GetAllTasksInput
    {
        public TaskState? State { get; set; }
    }
}
