using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Leapfrog.Roles.Dto;
using Leapfrog.Users.Dto;

namespace Leapfrog.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        System.Threading.Tasks.Task ChangeLanguage(ChangeUserLanguageDto input);

        Task<bool> ChangePassword(ChangePasswordDto input);
    }
}
