﻿using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Mvc.Razor.Internal;

namespace Leapfrog.Web.Views
{
    public abstract class LeapfrogRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected LeapfrogRazorPage()
        {
            LocalizationSourceName = LeapfrogConsts.LocalizationSourceName;
        }
    }
}
