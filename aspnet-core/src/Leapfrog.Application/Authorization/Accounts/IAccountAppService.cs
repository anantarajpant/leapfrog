﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Leapfrog.Authorization.Accounts.Dto;

namespace Leapfrog.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
