﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Leapfrog.Configuration;
using Leapfrog.Web;

namespace Leapfrog.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class LeapfrogDbContextFactory : IDesignTimeDbContextFactory<LeapfrogDbContext>
    {
        public LeapfrogDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<LeapfrogDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            LeapfrogDbContextConfigurer.Configure(builder, configuration.GetConnectionString(LeapfrogConsts.ConnectionStringName));

            return new LeapfrogDbContext(builder.Options);
        }
    }
}
