﻿namespace Leapfrog.Web.Views.Shared.Components.TenantChange
{
    public class ChangeModalViewModel
    {
        public string TenancyName { get; set; }
    }
}
