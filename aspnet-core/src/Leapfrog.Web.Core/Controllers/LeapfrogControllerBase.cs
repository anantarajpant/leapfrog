using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace Leapfrog.Controllers
{
    public abstract class LeapfrogControllerBase: AbpController
    {
        protected LeapfrogControllerBase()
        {
            LocalizationSourceName = LeapfrogConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
