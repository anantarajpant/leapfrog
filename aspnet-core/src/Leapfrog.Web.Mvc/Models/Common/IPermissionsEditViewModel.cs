﻿using System.Collections.Generic;
using Leapfrog.Roles.Dto;

namespace Leapfrog.Web.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }
    }
}