﻿using Abp.Application.Services;
using Leapfrog.MultiTenancy.Dto;

namespace Leapfrog.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

