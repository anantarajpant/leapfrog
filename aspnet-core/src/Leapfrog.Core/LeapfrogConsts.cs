﻿namespace Leapfrog
{
    public class LeapfrogConsts
    {
        public const string LocalizationSourceName = "Leapfrog";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
