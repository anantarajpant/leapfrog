﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Leapfrog.Authorization;

namespace Leapfrog
{
    [DependsOn(
        typeof(LeapfrogCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class LeapfrogApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<LeapfrogAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(LeapfrogApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
