﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using Leapfrog.Controllers;
using Leapfrog.Authorization;

namespace Leapfrog.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Roles)]
    public class HomeController : LeapfrogControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
